/*	-INFORMATION-

	Subject:	Spell
	Source: 	Deep Magic by the kobold press
	Author: 	Super9mega
	Sheet:  	V13.1.0 and newer
*/

var iFileName = "Deep Magic - Spells.js";
RequiredSheetVersion("13.1.0");

//source, Deep magic third party book by kobold press, 2020
//Corrections and errata's are included. Some personal in order to clarify spells. if theres something you see wrong tell me on github
SourceList["DPM"] = {
	name : "Deep Magic",
	abbreviation : "DPM",
	abbreviationSpellsheet : "DP",
	group : "Kobold Press",
	url : "https://koboldpress.com/kpstore/product/deep-magic-for-5th-edition/"
}

//cantrips in alphabetical order
SpellsList["animated scroll"] = {
	name : "Animated Scroll",
	classes : ["druid", "sorcerer", "wizard"],
	source : ["DPM", 38],
	level : 0,
	school : "Trans",
	time : "1 a",
	range : "Touch",
	components : "V,S,M",
	compMaterial : "intricately folded paper or parchment",
	duration : "24 h",
	description : "Fold paper into the shape of a CR 0 beast. It follows your command",
	descriptionFull : "The paper or parchment must be folded into the shape of an animal before casting the spell. It then becomes an animated paper animal of the kind the folded paper most closely resembles. The creature uses the stat block of any beast that has a challenge rating of 0. It is made of paper, not flesh and bone, but it can do anything the real creature can do: a paper owl can fly and attack with its talons, a paper frog can swim without disintegrating in water, and so fourth. It follows your commands to the best of its ability, including carrying messages to a recipient whose location you know." + "\n  " + "The duration increases by 24 hours at 5th level (48 hours), 11th level (72 hours), and 17th level (96 hours)."
};
SpellsList["bless the dead"] = {
    name : "Bless the Dead",
    source : ["DPM", 45],
    classes : ["cleric", "druid", "warlock"],
    level : 0,
    school : "Abjur",
    time : "1 a",
    range : "Touch",
    components : "V,S",
    duration : "Instantaneous",
    description : "Bless one deceased crea, passes over in peace, no effect on living crea/undead",
    descriptionFull : "You grant a blessing to one deceased creature, enabling it to cross over to the realm of the dead in peace. A creature that benefits from bless the dead can't become undead. The spell has no effect on living creatures or the undead."
}
SpellsList["blood tide"] = {
    name : "Blood Tide",
    source : ["DPM", 319],
    classes : ["sorcerer", "wizard"],
    level : 0,
    school : "Necro",
    time : "1 a",
    range : "25 ft",
    components : "V",
    duration : "4 rnd",
	save : "Con",
    description : "crea makes con saving throw, on fail, -2 to Int, Cha, Wis checks. might attract beasts.",
    descriptionFull : "When you cast this spell, a creature you designate within range must succeed on a Constitution saving throw or bleed from its nose, eyes, ears, and mouth. This bleeding deals no damage but imposes a -2 penalty on the creature's Intelligence, Charisma, and Wisdom checks. Blood tide has no effect on undead or constructs." + "\n  " + "A bleeding creature might attract the attention of creatures such as stirges, sharks, or giant mosquitoes, depending on the circumstances." + "\n  " + "A cure wounds spell stops the bleeding before the duration of blood tide expires, as does a successful DC 10 Wisdom (medicine) check." + "\n  " + "The spell's duration increases to 2 minutes when you reach 5th level, to 10 minutes when you reach 11th level, and to 1 hour when you reach 17th level."
}
SpellsList["memento mori"] = {
    name : "Memento Mori",
    source : ["DPM", 94],
    classes : ["cleric", "druid", "sorcerer", "warlock", "wizard"],
    level : 0,
    school : "Necro",
    time : "1 a",
    range : "5 ft",
    components : "V,S",
    duration : "1 rnd",
    save : "Cha",
    description : "Transform into vis. of death, Cha save or stunned until end of turn",
    descriptionFull : "You transform yourself into a horrifying vision of death, rotted and crawling with maggots, exuding the stench of the grave. Each creature within range that can see you must succeed on a Charisma saving throw or be stunned until the end of its next turn." + "\n " + "A creature that succeeds on the saving throw is immune to further castings of this spell for 24 hours."
}
SpellsList["shadow blindness"] = {
    name : "Shadow Blindness",
    source : ["DPM", 250],
    classes : ["warlock", "wizard"],
    level : 0,
    school : "Illus",
    time : "1 a",
    range : "Touch",
    components : "V,S",
    duration : "1 rnd",
    description : "Melee spell atk with cast against crea with darkvision;hit: target losses darkvision ",
    descriptionFull : "You make a melee spell attack against a creature you touch that has darkvision as an innate ability; on a hit, the target's darkvision is negated until the spell ends. This spell has no effect against darkvision that derives from a spell or a magic item. The target retains all of its other senses."
}
SpellsList["wind lash"] = {
    name : "Wind Lash",
    source : ["DPM", 120],
    classes : ["sorcerer", "warlock", "wizard"],
    level : 0,
    school : "Evoc",
    time : "1 a",
    range : "20 ft",
    components : "V,S",
    duration : "Instantaneous",
    description : "Melee spell atk; Hit: 1d8 Slashing dmg and push 5 ft away",
	descriptionCantripDie : "Melee spell atk; Hit: `CD`d8 Slashing dmg and push 5 ft away",
    descriptionFull : "Your swirft gesture creates a solid lash of howling wind. Make a melee spell attack against the target. On a hit, the target takes 1d8 slashing damage from the shearing wind and is pushed 5 feet away from you." + "\n  " + "The spell's damage increases by 1d8 when you reach 5th level (2d8), 11th level (3d8), and 17th level (4d8)."
}
WeaponsList["wind lash"] = {
	name : "Wind Lash",
	source : ["DPM", 120],
	type : "Cantrip",
	ability : 0,
	abilitytodamage : false,
	damage : ["C", 8, "slashing"],
	range : "20 ft",
	dc : false,
	useSpellcastingAbility : true,
	regExpSearch : /^(?=.*wind)(?=.*lash).*$/i,
	description : "On hit: Crea pushed 5 ft",
	list : "spell"
}
//1st level spells in alphabetical order
SpellsList["blinding pain"] = {
    name : "Blinding Pain",
    source : ["DPM", 45],
    classes : ["bard", "sorcerer", "warlock"],
    level : 1,
    school : "Ench",
    time : "1 a",
    range : "30 ft",
    components : "V,S,M",
    compMaterial : "an ice pick",
    duration : "1 min",
	save : "Wis",
    description : "At start of turn, target takes 1d4+1d4/SL and is blinded, on sucessful save target takes half and not blinded",
    descriptionFull : "You cause a creature within range to suffer severe pain in its head. At the start of its turn, the target takes 1d4 psychic damage and is blinded for 1 round. On a successful Wisdom saving throw, the target takes half as much damage and is not blinded. A cure wounds or healing word spell cast on the target ends this spell, in addition to its regular effects."
}
SpellsList["blood scarab"] = {
    name : "Blood Scarab",
    source : ["DPM", 318],
    classes : ["cleric", "warlock"],
    level : 1,
    school : "Necro",
    time : "1 a",
    range : "30 ft",
    components : "V,M",
	compMaterial : "A drop of the caster's blood, and the exoskeleton of a scarab beetle",
    duration : "Instantaneous",
	save : "Con",
    description : "Crea makes Con save, on fail, 1d6 Necro dmg. Temp HP equal to dmg. extra for each slot above 1",
    descriptionFull : "Your blood is absorbed into the beetle's exoskeleton to form a beautiful, rubylike scarab that fies toward a creature of your choice within range. The target must make a successful Constitution saving throw or take 1d6 necrotic damage, You gain temporary hit points equal to the necrotic damage delt." + AtHigherLevels + "When you cast this spell using a spell slot of 2nd level or higher, the number of scarabs increases by one for each slot level above 1st. You can direct the scarabs at the same target or at different targets. Each target makes a single saving throw, regardless of the number of scarabs targeting it."
}
SpellsList["bloody hands"] = {
    name : "Bloody Hands",
    source : ["DPM", 319],
    classes : ["sorcerer", "warlock", "wizard"],
    level : 1,
    school : "Necro",
    time : "1 a",
    range : "30 ft",
    components : "V,S",
    duration : "Conc, 1 min",
	save : "Con",
    description : "Crea makes con save each round;fail: take 1 Necro dmg and dis on atk rolls. DC 10 for V or M spells",
    descriptionFull : "You cause the hands (or other appropriate body parts, such as claws or tentacles) of a creature within range to bleed profusely. The target must succeed on a Constitution saving throw or take 1 necrotic damage each round and suffer disadvantage on all melee and ranged attack rolls that require the use of its hands for the spell's duration." + "\n  " + "Casting any spell that has somatic or material components while under the influence of this spell requires a DC 10 Constitution saving throw. An a failed save, the spell is not cast but it is not lost; the casting can be attempted again in the next round."
}
SpellsList["candles insight"] = {
    name : "Candle's insight",
    source : ["DPM", 47],
    classes : ["bard", "warlock", "wizard"],
    level : 1,
    school : "Div",
    time : "1 a",
    range : "10 ft",
    components : "V,S,M",
    compMaterial : "a blessed candle",
    duration : "10 min",
    description : "light candle, candle flickers if target misleads, goes out if target lies",
    descriptionFull : "Candle's insight is cast on its target as the component candle is lit. The candle burns for up to 10 minutes unless it's extinguished normally or by the spell's effect. While the candle burns, the caster can question the spell's target and the candle reveals whether the target speaks truthfully. An intentionally misleading or partial answer causes the flame to flicker and dim. An outright lie causes the flame to flare and then go out, ending the spell. The candle judges honesty, not absolute truth; the flame burns steadily through even an outrageously false statement, as long as the target believes it's true." + "\n " + "Candle's insight is used across society: by merchants while negotiating deals, by inquisitors investigating heresy, and by monarchsas they interview foreign diplomats. In some societies, casting candle's insight without the consent of the spell's target is considered a serious breach of hospitality."
}
SpellsList["feather field"] = {
    name : "Feather Field",
    source : ["DPM", 69],
    classes : ["druid", "ranger", "warlock", "wizard"],
    level : 1,
    school : "Abjur",
    time : "1 rea",
	timeFull : "1 reaction, which you take when you are targeted by a ranged attack from a magic weapon but before the attack roll is made",
    range : "Self",
    components : "V,S,M",
    compMaterial : "fletching from an arrow",
    duration : "1 rnd",
    description : "Magic barrier protects you. +5 AC against ranged attacks by magic weapons",
    descriptionFull : "A magical barrier of chaff in the form of feathers appears and protects you. Until the start of your next turn, you have a +5 bonus to AC against ranged attacks by magic weapons" + AtHigherLevels + "When you cast feather field using a spell slot of 2nd level or higher, the duration is increased by 1 round for each slot level above 1st."
}
SpellsList["hunters endurance"] = {
    name : "Hunter's Endurance",
    source : ["DPM", 81],
    classes : ["ranger", "warlock"],
    level : 1,
    school : "Ench",
    time : "1 min",
    range : "Self",
    components : "V,S,M",
    compMaterial : "a fingernail, lock of hair, bit of fur, or drop of blood from the target, if unfamiliar",
    duration : "24 h",
    description : "Describe or have a piece of crea, Adv. on perception and survival. must pursue target. see description.",
    descriptionFull : "You call on the land to sustain you as you hunt your quarry. Describe or name a creature that is familiar to you. If you aren't familiar with the target creature, you must use a fingernail, lock of hair, bit of fur, or drop of blood from it as a material component to target that creature with this spell." + "\n  " + "Until the spell ends, you have advantage on all Wisdom (Perception) and Wisdom (Survival) checks to find and track the target, and you must actively pursue the target as if under a geas. In addition, you don't suffer from exhausting level you gain from pursuing your quarry, such as from lack of rest or environmental hazards between you and the target, while the spell is active. When the spell ends, you suffer from all levels of exhaustion that were suspended by the spell. The spell ends only after 24 hours, when the target is dead, when the target is on a different plane, or when the target is restrained in your line of sight."
}
SpellsList["insightful maneuver"] = {
    name : "Insightful Maneuver",
    source : ["DPM", 84],
    classes : ["cleric", "paladin", "ranger", "sorcerer", "warlock", "wizard"],
    level : 1,
    school : "Div",
    time : "1 bns",
    range : "Touch",
    components : "V,S",
    duration : "Instantaneous",
    description : "Target has vulnerability to one dmg type of your choice, you learn all existing vulnerabilities",
    descriptionFull : "With a flash of insight, you know how to take advantage of your foe's vulnerabilities, Until the end of your turn, the target has vulnerability to one type of damage (your choice). Additionally, if the target has any other vulnerabilities, you learn them."
}
SpellsList["mosquito bane"] = {
    name : "Mosquito Bane",
    source : ["DPM", 95],
    classes : ["druid", "sorcerer", "warlock", "wizard"],
    level : 1,
    school : "Necro",
    time : "1 a",
    range : "50 feet",
    components : "V,S",
    duration : "Instantaneous",
    description : "kill all insects or swarms within range than have 25+15/SL hit points or fewer",
    descriptionFull : "This spell kills any insects or swarms of insects within range that have a total of 25 hit points or fewer." + AtHigherLevels + "When you cast this spell using a spell slot of 2nd level or higher, the number of hit points affected increases by 15 for each slot level above 1st. Thus, a 2nd-level spell kills insects or swarms that have up to 40 hit points, a 3rd-level spell kills those with 55 hit points or fewer, and so fourth, up to a maximum of 85 hit points for a slot of 6th level or higher."
}
SpellsList["stanch"] = {
    name : "Stanch",
    source : ["DPM", 321],
    classes : ["cleric", "druid", "ranger", "sorcerer", "warlock", "wizard"],
    level : 1,
    school : "Trans",
    time : "1 a",
    range : "Touch",
    components : "V,S",
    duration : "1 hour",
    description : "Stabilizes target and removes bleeding, target cant be source of blood",
    descriptionFull : "The target's blood coagulates rapidly, so that a dying target stabilizes and any ongoing bleeding or wounding effect on the target ends. The target can't be the source of blood for any spell or effect that requires even a drop of blood."
}
SpellsList["strength of an ox"] = {
    name : "Strength of an Ox",
    source : ["DPM", 111],
    classes : ["cleric", "paladin", "sorcerer", "warlock", "wizard"],
    level : 1,
    school : "Trans",
    time : "1 a",
    range : "Touch",
    components : "V,S,M",
    compMaterial : "A 1-pound weight",
    duration : "Conc, 1 min",
    description : "Touch 1 creature, Can carry, push, drag, or lift weight as if it was 1 size category larger. can carry unwieldy items",
    descriptionFull : "You touch a creature and give it the capacity to carry, push, drag, or lift weights as if it were one size category larger for the duration of the spell. The target is also not subject to the penalties given in the variant rules for encumbrance. Furthermore, the subject can carry a load that would normally be unwieldy, such as a large log, a rowboat, or an oxcart."
}
SpellsList["thin the ice"] = {
    name : "Thin the Ice",
    source : ["DPM", 113],
    classes : ["druid", "ranger", "sorcerer", "warlock", "wizard"],
    level : 1,
    school : "Trans",
    time : "1 a",
    range : "60 ft",
    components : "V,S,M",
    compMaterial : "A piece of sunstone",
    duration : "Instantaneous",
    description : "10 ft rad. of ice, 40 ft deep melts with thin layer of ice on top. Crea. 20 lbs. or more falls through. 2d6",
    descriptionFull : "You target a point within range, That point becomes the top center of a cylinder 10 feet in radius and 40 feet deep. All ice inside that area melts immediately. The uppermost layer of ice seems to remain intact and sturdy, but it covers a 40-foot-deep pit filled with ice water. A successful Wisdom (Survival) check or passive Perception check against your spell save DC notices the thin ice. If a creature weighing more than 20 pounds (or a greater weight specified by you when casting the spell) treads over the cylinder or is already standing on it, the ice gives way. Unless the creature makes a successful Dexterity saving throw, it falls into the icy water, taking 2d6 cold damage plus whatever other problems are caused by water, by armor, or by being drenched in a freezing environment. The water gradually refreezes normally."
}
SpellsList["trick question"] = {
    name : "Trick Question",
    source : ["DPM", 117],
    classes : ["bard", "druid", "sorcerer", "warlock", "wizard"],
    level : 1,
    school : "Ench",
    time : "1 a",
    range : "30 ft",
    components : "V,S",
    duration : "Instantaneous",
	save : "Wis",
    description : "Ask a question that can be answered by one word, Wis. save or Crea. must answer.",
    descriptionFull : "You pose a question that can be answered by one word, directed at a creature that can hear you. The target must make a successful wisdom saving throw or be compelled to answer your question truthfully. When the answer is given, the target knows that you used magic to compel it."
}
SpellsList["unluck on that"] = {
    name : "Unluck on That",
    source : ["DPM", 118],
    classes : ["bard", "cleric", "sorcerer", "warlock", "wizard"],
    level : 1,
    school : "Ench",
    time : "1 rea",
    timeFull : "1 reaction, which you take when a creature within range makes an attack roll, saving throw, or ability check",
    range : "25 ft",
    components : "V",
    duration : "Instantaneous",
    description : "Utter a swift curse, target gets disadvantage on the roll",
    descriptionFull : "By uttering a swift curse ('Unluck on that!'), you bring misfortune to the target's attempt; the affected creature has disadvantage on the roll." + AtHigherLevels + "When you cast this spell using a spell slot of 2nd level or higher, the range of the spell increases by 5 feet for each slot level above 1st."
}
SpellsList["weapon of blood"] = {
    name : "Weapon of Blood",
    source : ["DPM", 321],
    classes : ["sorcerer", "wizard"],
    level : 1,
    school : "Trans",
    time : "1 a",
    range : "Self",
    components : "V,S,M",
	compMaterial : "a pinch of iron shavings",
    duration : "conc, 1 h",
    description : "inflict 1d4 slash dmg, cannot heal while spell is active, gives +1 dagger",
    descriptionFull : "When you cast this spell, you inflict 1d4 slashing damage on yourself that can't be healed until after the blade created by this spell is destroyed or the spell ends. The trickling blood transforms into a dagger of read metal that functions as a +1 dagger." + AtHigherLevels + "When you cast this spell using a spell slot of 3rd to 5th level, the self-inflicted wound deals 3d4 slashing damage and the spell produces a +2 dagger. When you cast this spell using a spell slot of 6th to 8th level, the self-inflicted wound deals 6d4 slashing damage and the spell produces a +2 dagger of wounding. When you cast this spell using a 9th-level spell slot, the self-inflicted wound deals 9d4 slashing damage nad the spell produces a +3 dagger of wounding."
}
SpellsList["writhing arms"] = {
    name : "Writhing Arms",
    source : ["DPM", 122],
    classes : ["sorcerer", "warlock", "wizard"],
    level : 1,
    school : "Trans",
    time : "1 a",
    range : "10 ft",
    components : "V,S",
    duration : "Conc, 1 min",
    description : "Melee spell atk. Hit: 1d10+1d10/SL plus grappled, escape DC is spell save. target grappled: no atk roll",
    descriptionFull : "Your arms become constantly writhing tentacles, You can use your action to make a melee spell attack against any target within range. The target takes 1d10 necrotic damage and is grappled (escape DC is your spell save DC). If the target does not escape your grapple, you can use your action on each subsequent turn to deal 1d10 necrotic damage to the target automatically." + AtHigherLevels + "When you cast this spell using a spell slot of 2nd level or higher, the damage you deal with your tentacle attack increases by 1d10 for each slot level above 1st."
}

//2nd level spells in alphabetical order
SpellsList["altheas travel tent"] = {
	name : "Althea's Travel Tent",
	classes : ["todo"],
	source : ["DPM", 35],
	level : 2,
	school : "Conj",
	time : "5 min",
	range : "Touch",
	components : "V,S,M",
	duration : "8 h",
	description : "Touch a canvas tent, inside becomes larger and houses a desk and bed",
	descriptionFull : "You touch an ordinary, properly pitched canvas tent to create a space whee you and a companion can sleep in comfort. From the outside, the tent appears normal, but inside it has a small foyer and a larger bedchamber. The poyer contains a writing desk with a chail; the bedchamber holds a soft bed large enough to sleep two, a small nightstand with a candle, and a small clothes rack. The floor of both rooms is a clean, dry, hard-packed version of the local ground. When the spell ends, the tent and the ground return to normal, and any creatures inside the tent are expelled to the nearest unoccupied spaces." + AtHigherLevels + "When the spell is cast using a 3rd-level slot, the poyer becomes a dining area with seats for six and enough floor space for six people to sleep, if they bring their own bedding. The sleeping room is unchanged. With a 4th-level slot, the temperature inside the tent is comfortable regardless of the outside temperature, and the dining area includes a small kitchen. With a 5th-level slot, an unseen servant is conjured to prepare and serve food (from your supplies). With a 6-th level slot, a third room is added that has three two-person beds. With a slot of 7th level or higher, the dining area and second sleeping area can each accommodate eight persons.",
	ritual : true
};
SpellsList["caustic blood"] = {
    name : "Caustic Blood",
    source : ["DPM", 320],
    classes : ["druid", "ranger", "sorcerer", "warlock", "wizard"],
    level : 2,
    school : "Trans",
    time : "1 rea",
	timeFull : "1 reaction, Which you take when an enemy's attack deals piercing or slashing damage to you",
    range : "Self",
    components : "V,S",
    duration : "Instantaneous",
	save : "Dex",
    description : "When you take pierce or slash, react to hurt three crea within 30 ft,dex save, fail: 1d10",
    descriptionFull : "Your blood becomes caustic when exposed to the air. When you take piercing or slashing damage, you can use your reaction to select up to three creatures within 30 feet of you. Each target takes 1d10 acid damage unless it makes a successful Dexterity saving throw." + AtHigherLevels + "When you cast this spell using a spell slot of 3rd level or higher, the number of targets increases by one for each slot level above 2nd, to a maximum of six targets"
}

//3rd level spells in alphabetical order
SpellsList["blood armor"] = {
    name : "Blood Armor",
    source : ["DPM", 318],
    classes : ["sorcerer", "warlock", "wizard"],
    level : 3,
    school : "Necro",
    time : "1 bns",
    range : "Self",
    components : "V,S",
	compMaterial : "you must have just struck a foe with a melee weapon",
    duration : "1 h",
    description : "Strike crea with melee wea, grants caster AC 18 + dex. if crea is celestial, adv on Cha saves",
    descriptionFull : "When you strike a foe with a melee weapon attack, you can immediately cast blood armor as a bonus action. The foe you struck must contain blood; if the target doesn't bleed, the spell ends without effect. The blood flowing from your foe magically increases in volume and forms a suit of armor around you. granting you an Armor Class of 18 + your Dexterity modifier for the spells duration. This armor has no Strength requirement, doesn't hinder spellcasting, and doesn't incur disadvantage on Dexterity (Stealth) Checks." + "\n  " + "If the creature you struck was a celestial, blood armor also grants you advantage on Charisma saving throws for the duration of the spell."
}

//4th level spells in alphabetical order
SpellsList["blood spoor"] = {
    name : "Blood Spoor",
    source : ["DPM", 318],
    classes : ["ranger"],
    level : 4,
    school : "Div",
    time : "1 a",
    range : "Self",
    components : "V,S,M",
	compMaterial : "a drop of the quarry's blood",
    duration : "conc, 10 min",
    description : "Touch drop of blood, you can follow trail except across planes, unless otherwise noted",
    descriptionFull : "By touching a drop of your quarry's blood (spilled or drawn within the past hour), you can follow the creature's trail unerringly across any surface or under water, no matter how fast you are moving, If your quarry takes flight, you can follow its trail along the ground-or through the air. if you have the means to fly." + "\n  " + "If your quarry moves magically (such as by way of a dimension door or a teleport spell), you sense its trail as a staight path leading from where the magical movement started to where it ended. Such a route might lead through lethal or impassable barriers. This spell even reveals the route of a creature using pass without trace, but it fails to locate a creature protected by nondetection or by other effects that prevent scying spells or cause divination spells to fail. If your quarry moves to another plane, its trail ends without trace, but blood spoor picks up the trail again if the caster moves to the same plane as the quarry before the spell's duration expires."
}

//holding zone for future spells
