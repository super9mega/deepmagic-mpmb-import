# Super's More Purple More Better's imports
This is a repository for everything that I plan on adding into the character sheet from various third party sources.
This project is done in my free time and without pay from any community.
If you see any errors or misspellings please put in an issue and/or a merge to fix it. 
Will add Fightclub XML after finishing the Javascript versions

## More Purple More Better's Imports for Deep Magic
Kobold press released the book in October 6, 2020. 
This document is to import all the spells into the character sheet under the group "Kobold Press".
Not currently implemented yet. Will add subclasses later once I finish putting everything into the sheet.
currently there are only 25 spells added fully. 

- [ ] Add Every Spell
  - [ ] Main list
  - [ ] Clockwork Magic
  - [ ] Rune Magic
  - [ ] Void Magic
  - [ ] Illumination Magic
  - [ ] Ley Lines 
  - [ ] Angelic Seals
  - [ ] Chaos Magic
  - [ ] Battle Magic
  - [ ] Ring Magic
  - [ ] Shadow Magic
  - [ ] High Elvish Magic
  - [ ] Blood & Doom
  - [ ] Dragon Magic
  - [ ] Elemental Magic
  - [ ] Heiroglyphic Magic
  - [ ] Time Magic
  - [ ] Mythos Magic
  - [ ] Combat Divination
  - [ ] Winter Magic
  - [ ] Alkemancy
- [ ] Add Subclasses
  - [ ] Todo
  - [ ] Todo
  - [ ] Todo
- [ ] Add Extra Schools
- [ ] Add cantrips as weapons
